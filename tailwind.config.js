/** @type {import('tailwindcss').Config} */
module.exports = {
  content: { 
    files: ["*.html", "./core/web/**/*.rs"],
  },
  theme: {
    extend: {},
  },
  plugins: [],
}
