pub mod app;
pub mod component;
pub mod page;
use cfg_if::cfg_if;

cfg_if! {
if #[cfg(feature = "hydrate")] {

    use wasm_bindgen::prelude::wasm_bindgen;
    #[wasm_bindgen]
    pub fn hydrate() {
        use crate::app::*;
        use leptos::*;

        leptos::mount_to_body(move |cx| {
            view! { cx, <App/> }
        });
    }
}
}
