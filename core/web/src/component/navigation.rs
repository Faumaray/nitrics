use leptos::{
    component, create_signal, view, IntoView, Scope, Show, ShowProps, SignalGet, SignalUpdate,
    WriteSignal,
};
use leptos_router::*;

#[component]
pub fn Nav(cx: Scope, toggle_sidebar: WriteSignal<bool>) -> impl IntoView {
    view! {
     cx,
        <>
            <header aria-label="Site Header" class="shadow-sm sticky z-40 w-full bg-gray-400 ">
              <div class="mx-auto max-w-screen-xl p-4">
                <div class="flex items-center justify-between gap-4 lg:gap-10">
                  <div class="flex lg:w-0 lg:flex-1 mr-auto">
                    <button class="rounded-lg bg-gray-100 p-2 text-gray-600" type="button" on:click=move |_| toggle_sidebar.update(|sidebar| *sidebar = !(*sidebar))>
                      <span class="sr-only">"Open menu"</span>
                      <svg
                        aria-hidden="true"
                        class="h-5 w-5"
                        fill="none"
                        stroke="currentColor"
                        viewbox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4 6h16M4 12h16M4 18h16"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                          stroke-width="2"
                        />
                      </svg>
                    </button>
                  </div>

                  <form class="mb-0 hidden lg:flex">
                    <div class="relative">
                      <input
                        class="h-10 rounded-lg border-gray-200 pe-10 text-sm placeholder-gray-300 focus:z-10"
                        placeholder="Search..."
                        type="text"
                      />

                      <button
                        type="submit"
                        class="absolute inset-y-0 end-0 rounded-r-lg p-2 text-gray-600"
                      >
                        <span class="sr-only">"Submit Search"</span>
                        <svg
                          class="h-5 w-5"
                          fill="currentColor"
                          viewbox="0 0 20 20"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            clip-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            fill-rule="evenodd"
                          ></path>
                        </svg>
                      </button>
                    </div>
                  </form>

                  <div class="hidden flex-1 items-center justify-end gap-4 ml-auto sm:flex">
                    <A
                      class="rounded-lg bg-gray-100 px-5 py-2 text-sm font-medium text-gray-500"
                      href=""
                    >
                      "Log in"
                    </A>

                    <A
                      class="rounded-lg bg-blue-600 px-5 py-2 text-sm font-medium text-white"
                      href=""
                    >
                      "Sign up"
                    </A>
                  </div>
                </div>
              </div>
            </header>

        </>
    }
}

#[component]
pub fn side_bar(cx: Scope) -> impl IntoView {
    view! {cx,
           <div class="w-1/6 absolute z-30 inset-y-0 left-0 p-4 bg-gray-200 h-screen flex-col justify-between border-e ">
     <div class="px-4 py-6">
       <nav aria-label="Main Nav" class="mt-6 flex flex-col space-y-1">
         <a
           href="#"
           class="flex items-center gap-2 rounded-lg bg-gray-100 px-4 py-2 text-gray-700"
         >
           <svg
             xmlns="http://www.w3.org/2000/svg"
             class="h-5 w-5 opacity-75"
             fill="none"
             viewBox="0 0 24 24"
             stroke="currentColor"
             stroke-width="2"
           >
             <path
               stroke-linecap="round"
               stroke-linejoin="round"
               d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
             />
             <path
               stroke-linecap="round"
               stroke-linejoin="round"
               d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
             />
           </svg>

           <span class="text-sm font-medium"> "General" </span>
         </a>

         <details class="group [&_summary::-webkit-details-marker]:hidden">
           <summary
             class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
           >
             <div class="flex items-center gap-2">
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                 />
               </svg>

               <span class="text-sm font-medium"> "Teams" </span>
             </div>

             <span class="shrink-0 transition duration-300 group-open:-rotate-180">
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5"
                 viewBox="0 0 20 20"
                 fill="currentColor"
               >
                 <path
                   fill-rule="evenodd"
                   d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                   clip-rule="evenodd"
                 />
               </svg>
             </span>
           </summary>

           <nav aria-label="Teams Nav" class="mt-2 flex flex-col px-4">
             <a
               href="#"
               class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
             >
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M18.364 18.364A9 9 0 005.636 5.636m12.728 12.728A9 9 0 015.636 5.636m12.728 12.728L5.636 5.636"
                 />
               </svg>

               <span class="text-sm font-medium"> "Banned Users" </span>
             </a>

             <a
               href="#"
               class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
             >
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                 />
               </svg>

               <span class="text-sm font-medium"> "Calendar" </span>
             </a>
           </nav>
         </details>

         <details class="group [&_summary::-webkit-details-marker]:hidden">
           <summary
             class="flex cursor-pointer items-center justify-between rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
           >
             <div class="flex items-center gap-2">
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
                 />
               </svg>

               <span class="text-sm font-medium"> "Account" </span>
             </div>

             <span class="shrink-0 transition duration-300 group-open:-rotate-180">
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5"
                 viewBox="0 0 20 20"
                 fill="currentColor"
               >
                 <path
                   fill-rule="evenodd"
                   d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                   clip-rule="evenodd"
                 />
               </svg>
             </span>
           </summary>

           <nav aria-label="Account Nav" class="mt-2 flex flex-col px-4">
             <a
               href="#"
               class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
             >
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2"
                 />
               </svg>

               <span class="text-sm font-medium">"Details" </span>
             </a>

             <a
               href="#"
               class="flex items-center gap-2 rounded-lg px-4 py-2 text-gray-500 hover:bg-gray-100 hover:text-gray-700"
             >
               <svg
                 xmlns="http://www.w3.org/2000/svg"
                 class="h-5 w-5 opacity-75"
                 fill="none"
                 viewBox="0 0 24 24"
                 stroke="currentColor"
                 stroke-width="2"
               >
                 <path
                   stroke-linecap="round"
                   stroke-linejoin="round"
                   d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"
                 />
               </svg>

               <span class="text-sm font-medium">"Security" </span>
             </a>

           </nav>
         </details>
       </nav>
     </div>
    </div>
       }
}
