use crate::component::navigation::*;
use crate::page::error::*;
use leptos::*;
use leptos_meta::*;
use leptos_router::*;

#[component]
pub fn App(cx: Scope) -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context(cx);

    let (sidebar, toggle_sidebar) = create_signal(cx, false);
    view! {
        cx,
        <>
            // injects a stylesheet into the document <head>
            // id=leptos means cargo-leptos will hot-reload this stylesheet
            <Stylesheet id="leptos" href="/style/output.css"/>
            <Link rel="shortcut icon" type_="image/ico" href="/style/favicon.ico"/>
            // sets the document title
            <Title text="Nitrics"/>
            // content for this welcome page
            <Router >
                    <div class="flex flex-col h-screen">
                             <Show
                                when=move || sidebar.get()
                                fallback=|cx| view! { cx,<></>}
                            >
                                <SideBar/>
                            </Show>
                            <Nav toggle_sidebar=toggle_sidebar/>


                            <main class="w-full text-center">
                                <Routes>
                                    <Route path="" view=|cx| view! { cx, <HomePage /> }/>
                                </Routes>
                            </main>
                        <footer/>
                    </div>
            </Router>
        </>
    }
}

/// Renders the home page of your application.
#[component]
fn HomePage(cx: Scope) -> impl IntoView {
    // Creates a reactive value to update the button
    let (count, set_count) = create_signal(cx, 0);
    let on_click = move |_| set_count.update(|count| *count += 1);
    view! { cx,
            <h2 class="p-6 text-4xl">"Welcome to Leptos with Tailwind"</h2>
            <p class="px-10 pb-10 text-left">"Tailwind will scan your Rust files for Tailwind class names and compile them into a CSS file."</p>
            <button
                class="bg-amber-600 hover:bg-sky-700 px-5 py-3 text-white rounded-lg"
                on:click=on_click
            >
                "Something's here | "
                {move || if count() == 0 {
                    "Click me!".to_string()
                } else {
                    count().to_string()
                }}
                " | Some more text"
            </button>
    }
}
