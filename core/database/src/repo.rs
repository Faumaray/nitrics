use serde::{Deserialize, Serialize};
use std::sync::Arc;

use surrealdb::engine::remote::ws::{Client, Ws};
use surrealdb::Error;
use surrealdb::Surreal;
#[derive(Clone)]
pub struct DatabaseConnection {
    pub db: Arc<Surreal<Client>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DatabaseConfig<T: AsRef<str>> {
    pub url: T,
    pub namespace: T,
    pub table: T,
    pub credentials: Credentials<T>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Credentials<T: AsRef<str>> {
    pub username: T,
    pub password: T,
}
// TODO: FIXME: Replace split init function with auth to own namespace and table for each client
// after jwt auth
impl DatabaseConnection {
    pub async fn init<T>(config: DatabaseConfig<T>) -> Result<Self, Error>
    where
        T: AsRef<str> + Serialize,
    {
        let db = Surreal::new::<Ws>(config.url.as_ref()).await?;

        db.signin(surrealdb::opt::auth::Database {
            namespace: config.namespace.as_ref(),
            database: config.table.as_ref(),
            username: config.credentials.username.as_ref(),
            password: config.credentials.password.as_ref(),
        })
        .await?;
        db.use_ns(config.namespace.as_ref())
            .use_db(config.table.as_ref())
            .await?;

        Ok(Self { db: Arc::new(db) })
    }
}
