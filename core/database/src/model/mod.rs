/* FIXME: Rewrite
CREATE TABLE Contact(
  id INT PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255),
  phone VARCHAR(20)
);
*/
pub mod contact;

pub mod person;

/*
CREATE TABLE Lead (
  id INT PRIMARY KEY,
  contact_id INT,
  status VARCHAR(20),
  notes VARCHAR(255),
  FOREIGN KEY(contact_id) REFERENCES Contact(id)
);
*/
pub mod lead;

/*
CREATE TABLE Task (
  id INT PRIMARY KEY,
  title VARCHAR(255),
  due_date DATE,
  completed BOOLEAN
);
*/
pub mod task;

/*
CREATE TABLE Product (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  description VARCHAR(255),
  price FLOAT
);
*/
pub mod product;
