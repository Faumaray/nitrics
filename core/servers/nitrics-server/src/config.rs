use config::{Config, ConfigError, Environment, File};
use serde::{Deserialize, Serialize};
use tracing::{info, info_span};

#[derive(Deserialize, Debug)]
pub struct ApiServer {
    pub ip: String,
    pub port: u32,
}

#[derive(Deserialize, Debug)]
pub struct SiteServerConfig<T: AsRef<str>> {
    pub ip: T,
    pub port: u32,
    pub serving_folder: T,
    pub serving_name: T,
}
#[derive(Serialize, Deserialize, Debug)]
pub struct DatabaseConfig<T: AsRef<str>> {
    pub url: T,
    pub namespace: T,
    pub table: T,
    pub credentials: Credentials<T>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Credentials<T: AsRef<str>> {
    pub username: T,
    pub password: T,
}

#[derive(Deserialize, Debug)]
pub struct Settings {
    pub database: DatabaseConfig<String>,
    pub api_server: ApiServer,
    pub site_server: SiteServerConfig<String>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let span = info_span!("Settings");
        let _enter = span.enter();
        let s = Config::builder()
            .add_source(File::with_name("settings"))
            .add_source(Environment::with_prefix("app"))
            .build()?;

        info!(
            "Loaded ENV(Databse): {:#?}",
            s.get::<DatabaseConfig<String>>("database")
        );
        info!(
            "Loaded ENV(Api Server): {:#?}",
            s.get::<ApiServer>("api_server")
        );
        info!(
            "Loaded ENV(Serving Site Server): {:#?}",
            s.get::<SiteServerConfig<String>>("site_server")
        );

        s.try_deserialize()
    }
}
