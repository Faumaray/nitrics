use opentelemetry::sdk::Resource;
use opentelemetry::KeyValue;
use opentelemetry::{global, runtime::Tokio, sdk::propagation::TraceContextPropagator};
use opentelemetry_otlp::{ExportConfig, WithExportConfig};
use tracing_appender::rolling;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::fmt::writer::MakeWriterExt;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::Layer;
mod config;

use crate::config::ApiServer;
use crate::config::SiteServerConfig;

use self::config::DatabaseConfig;

async fn load_serving_server(
    server_name: &str,
    config: SiteServerConfig<String>,
) -> std::result::Result<(), Box<dyn std::error::Error>> {
    unsafe {
        let lib = libloading::Library::new("./libserving_server.so")?;
        let func: libloading::Symbol<unsafe fn(&str, u32, &str, &str) -> std::io::Result<()>> =
            lib.get(b"serve")?;
        func(
            &config.ip,
            config.port,
            &config.serving_folder,
            &config.serving_name,
        )?;
    }
    Ok(())
}

#[tokio::main]
async fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    global::set_text_map_propagator(TraceContextPropagator::new());
    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint("http://localhost:4317"),
        )
        .with_trace_config(
            opentelemetry::sdk::trace::config().with_resource(Resource::new(vec![KeyValue::new(
                opentelemetry_semantic_conventions::resource::SERVICE_NAME,
                "Nitrics",
            )])),
        )
        .install_batch(opentelemetry::runtime::Tokio)?;
    let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    // Log all `tracing` events to files prefixed with `debug`. Since these
    // files will be written to very frequently, roll the log file every minute.
    let debug_file = rolling::hourly("./logs", "debug").with_min_level(tracing::Level::WARN);
    // Log warnings and errors to a separate file. Since we expect these events
    // to occur less frequently, roll that file on a daily basis instead.
    let warn_file = rolling::hourly("./logs", "warnings")
        .with_min_level(tracing::Level::WARN)
        .with_max_level(tracing::Level::ERROR);
    let info_file = rolling::hourly("./logs", "info")
        .with_max_level(tracing::Level::WARN)
        .with_min_level(tracing::Level::INFO);
    let all_files = debug_file.and(warn_file).and(info_file);
    let fmt = tracing_subscriber::fmt::Layer::new()
        .with_thread_names(true)
        .with_thread_ids(true)
        .compact()
        .with_writer(std::io::stdout)
        .with_filter(LevelFilter::INFO);

    let files = tracing_subscriber::fmt::Layer::new()
        .with_thread_ids(true)
        .with_thread_names(true)
        .with_line_number(true)
        .json()
        .with_writer(all_files);

    let collector = tracing_subscriber::registry::Registry::default()
        .with(fmt)
        .with(files)
        .with(telemetry);
    tracing::subscriber::set_global_default(collector).expect("Unable to set a global collector");
    let settings = config::Settings::new()?;
    tracing::info!("OpenTelementy at Port:6831");
    load_serving_server("", settings.site_server).await?;
    // Ensure all spans have been shipped to Jaeger.
    opentelemetry::global::shutdown_tracer_provider();
    Ok(())
}
