use ::web::app::*;
use actix_files::Files;
use actix_web::middleware::Compress;
use actix_web::*;
use leptos::*;
use leptos_actix::{generate_route_list, LeptosRoutes};
use rustls::{Certificate, PrivateKey, ServerConfig};
use rustls_pemfile::{certs, pkcs8_private_keys};
use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use tracing::{debug, debug_span, error, info, info_span};

#[derive(Deserialize, Debug)]
pub struct SiteServerConfig<'a> {
    pub ip: &'a str,
    pub port: u32,
    pub serving_folder: &'a str,
    pub serving_name: &'a str,
}

#[no_mangle]
pub fn serve(ip: &str, port: u32, serving_folder: &str, serving_name: &str) -> std::io::Result<()> {
    let span = info_span!("SSL certificates");
    let _enter = span.enter();

    let conf = leptos_config::LeptosOptions {
        output_name: serving_name.to_string(),
        site_root: serving_folder.to_string(),
        site_pkg_dir: String::from("pkg"),
        reload_port: port,
        env: if cfg!(debug) {
            leptos_config::Env::DEV
        } else {
            leptos_config::Env::PROD
        },
        site_addr: format!("{}:{}", ip, port).parse().unwrap(),
    };
    let addr = conf.site_addr;
    // Generate the list of routes in your Leptos App
    let routes = generate_route_list(|cx| view! { cx, <App/> });
    actix_web::rt::System::new().block_on(async {
        let mut server = HttpServer::new(move || {
            let leptos_options = &conf;
            let site_root = &leptos_options.site_root;

            App::new()
                .leptos_routes(
                    leptos_options.to_owned(),
                    routes.to_owned(),
                    |cx| view! { cx, <App/> },
                )
                .service(Files::new("/", site_root))
                .wrap(tracing_actix_web::TracingLogger::default())
                .wrap(Compress::default())
        });

        if let Ok(tls) = load_rustls_config() {
            server = server.bind_rustls(&addr, tls).unwrap();
            info!("Running HTTPS at {}", addr);
        } else {
            server = server.bind(&addr).unwrap();
                        info!("Running HTTP at {}", addr);
        }
        server.run().await.unwrap();
    });
    Ok(())
}

fn load_rustls_config() -> Result<rustls::ServerConfig, Box<dyn Error>> {
    let span = debug_span!("SSL certificates");
    let _enter = span.enter();
    // init server config builder with safe defaults
    let config = ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth();
    debug!("Created config without client auth");
    // load TLS key/cert files
    let cert_file = &mut BufReader::new(File::open("cert.pem")?);
    debug!("Readed certificate file");
    let key_file = &mut BufReader::new(File::open("key.pem")?);

    debug!("Readed certificate key file");
    // convert files to key/cert objects
    let cert_chain = certs(cert_file)?.into_iter().map(Certificate).collect();
    let mut keys: Vec<PrivateKey> = pkcs8_private_keys(key_file)?
        .into_iter()
        .map(PrivateKey)
        .collect();

    // exit if no keys could be parsed
    if keys.is_empty() {
        return Err(Box::new(rustls::Error::General(String::from(
            "Could not load TLS keys",
        ))));
    }

    Ok(config.with_single_cert(cert_chain, keys.remove(0))?)
}
